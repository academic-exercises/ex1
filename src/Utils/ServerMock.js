import Shirt1 from "../assets/shirt-1.webp";
import Shirt2 from "../assets/shirt-2.webp";
import Shirt3 from "../assets/shirt-3.webp";
import Shirt4 from "../assets/shirt-4.webp";

import Shirt1B from "../assets/shirt-1-2.webp";
import Shirt2B from "../assets/shirt-2-2.webp";
import Shirt3B from "../assets/shirt-3-2.webp";
import Shirt4B from "../assets/shirt-4-2.webp";

const StoreItems = [
  {
    name: "Classic Black",
    price: 3020,
    image: Shirt1,
    image2: Shirt1B,
    description: "This is a classic black shirt",
    store: "American Egle",
  },
  {
    name: "Classic Nike",
    price: 720,
    image: Shirt2,
    image2: Shirt2B,
    description: "This is a classic Nike shirt",
    store: "Nike",
  },
  {
    name: "Just Cool",
    price: 10000,
    image: Shirt3,
    image2: Shirt3B,
    description: "This is a cool shirt",
    store: "Smash",
  },
  {
    name: "Classic Boss",
    price: 3040,
    image: Shirt4,
    image2: Shirt4B,
    description: "This is a classic Boss shirt",
    store: "Addidas",
  },
];

const getData = () => {
  return StoreItems;
};

const getItemByIndex = (index) => {
  return StoreItems[index];
};

const ServerMock = {
  getData,
  getItemByIndex,
};

export default ServerMock;
