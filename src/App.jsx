import { Route, Routes } from "react-router-dom";

import HomePage from "./Pages/HomePage/HomePage";
import ItemPage from "./Pages/ItemPage/ItemPage";

const App = () => {
  return (
    <Routes>
      <Route path="/" caseSensitive={false} element={<HomePage />} />
      <Route path="/item/:index" element={<ItemPage />} />
    </Routes>
  );
};

export default App;
