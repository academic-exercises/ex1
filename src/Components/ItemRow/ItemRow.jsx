import useStyles from "./styles";
import { Grid } from "@mui/material";
import { AddCommas } from "../../Utils/General";
import { useNavigate } from "react-router-dom";

const ItemRow = ({ item, index }) => {
  const classes = useStyles();
  const navigate = useNavigate();

  return (
    <Grid
      key={index}
      container
      alignItems="center"
      style={{
        border: "2px solid black",
        padding: 12,
        margin: 12,
        borderRadius: 12,
      }}
      onClick={() => navigate(`/item/${index}`)}
    >
      <Grid item xs={6}>
        <h3>{item.name}</h3>
        <h3>{AddCommas(item.price)}$</h3>
      </Grid>
      <Grid item xs={6}>
        <img className={classes.image} src={item.image} alt={item.name} />
      </Grid>
    </Grid>
  );
};

export default ItemRow;
