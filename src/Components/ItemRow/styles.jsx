import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    height: "100vh",
  },
  title: {
    textAlign: "center",
  },
  image: {
    objectFit: "contain",
    borderRadius: 12,
    maxHeight: "30vh",
  },
});

export default useStyles;
