import useStyles from "./styles";
import { Grid } from "@mui/material";

import ServerMock from "../../Utils/ServerMock";
import ItemRow from "../../Components/ItemRow/ItemRow";

const HomePage = () => {
  const classes = useStyles();

  return (
    <Grid
      className={classes.root}
      container
      justifyContent="center"
      alignItems="center"
    >
      <Grid item xs={12} className={classes.title}>
        <h1>Buy List</h1>
      </Grid>
      <Grid item xs={5} className={classes.title}>
        <Grid container justifyContent="center" alignItems="center" spacing={2}>
          {ServerMock.getData().map((item, index) => (
            <ItemRow item={item} index={index} />
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default HomePage;
