import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    height: "100vh",
  },
  title: {
    textAlign: "center",
  },
  image: {
    objectFit: "cover",
    borderRadius: 12,
    maxHeight: "60vh",
    width: "15vw",
  },
});

export default useStyles;
