import useStyles from "./styles";
import { useParams } from "react-router-dom";
import { Grid } from "@mui/material";
import ServerMock from "../../Utils/ServerMock";
import { AddCommas } from "../../Utils/General";

const ItemPage = () => {
  const classes = useStyles();
  const params = useParams();
  const item = ServerMock.getItemByIndex(params.index);

  return (
    <Grid
      className={classes.root}
      container
      justifyContent="center"
      alignItems="start"
    >
      <Grid item xs={12} className={classes.title}>
        <h1>{item.name}</h1>
      </Grid>
      <Grid item xs={10} className={classes.title}>
        <Grid container justifyContent="center" alignItems="center" spacing={2}>
          <Grid container alignItems="center" spacing={2}>
            <Grid
              item
              xs={6}
              style={{
                textAlign: "left",
              }}
            >
              <h3>{item.name}</h3>
              <h3>{AddCommas(item.price)}$</h3>
              <h4>More Details: {item.description}</h4>
              <h4>Sold At: {item.store}</h4>
            </Grid>
            <div
              style={{
                flexGrow: 1,
              }}
            />
            <Grid item>
              <img className={classes.image} src={item.image} alt={item.name} />
            </Grid>
            <Grid item>
              <img
                className={classes.image}
                src={item.image2}
                alt={item.name}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ItemPage;
